require 'rails_helper'

RSpec.feature "Creating Article" do
	scenario "A user creates a new article" do 
		visit "/"
		click_link "New Article"
		fill_in "Title", with: "Creating a blog"
		fill_in "Body", with: "My new article"
		click_button "create article"
		expect(page).to have_content("Success")
		expect(page.current_path).to eq(articles_path)
	end
end 